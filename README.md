My own progress to learn all aspects to become a great Site Reliability Engineer - loosely based on [SRE University](https://github.com/andrealmar/sre-university) over on a GitHub repo.

**Objective**: obtain a complete understanding of the topics in order to become a successful SRE. Also to discover a specific field of study in order to do in depth research.



### SRE
- [X] Linux Academy's [Reliability Engineering Concepts](https://linuxacademy.com/profile/u/cert/id/385888)
- [ ] Google book **IN PROGRESS**
- [ ] A Cloud Guru's GCP DevOps Engineer Track **IN PROGRESS**
- [X] [GCP DevOps Engineer Track Part 1](https://linuxacademy.com/profile/u/cert/id/386627)
- [ ] Part 2 **IN PROGRESS**

### Human Behavior
- Incidence Response
- Cognitive Systems Engineering


### DevOps
- [ ] CI/CD


### Operating Systems
- [ ] Intro to OS on Udacity by Georgia Tech
- [ ] Advanced OS on Udacity by Georgia Tech 

- [ ] Absolute FreeBSD book **IN PROGRESS**
- [ ] How Linux Works book **IN PROGRESS**
- [ ] advanced Linux kernel

### Automation
- [ ] Terraform
- [ ] Ansible
- [ ] HashiStack
- [ ] Puppet


### Distributed Systems

- [ ] [MIT 6.824: Distributed Systems lectures](https://www.youtube.com/watch?v=cQP8WApzIQQ&list=PLrw6a1wE39_tb2fErI4-WkMbsvGQk9_UB)  **IN PROGRESS**
- [ ] book


### Networking
- [ ] IP Addresses and Subnetting course **IN PROGRESS**
- [X] DNS
- [ ] Understanding Linux Network Internals book
- [ ] Dave Crabble's [YouTube playlist](https://www.youtube.com/playlist?list=PLp0-EyBvv1bF9TwGsIzQUM2Pnhef3ixs0)
- [ ] DJ Ware's Linux Internals: Networking [presentation](https://youtu.be/KobYWL1LtW4)

### Security
- [ ] [Computer Systems Security course lectures from MIT](https://www.youtube.com/watch?v=GqmQg-cszw4&list=PLUl4u3cNGP62K2DjQLRxDNRi0z2IRWnNh)
- [ ] Dave Crabble's [Intro to Digital Certificates](https://youtu.be/qXLD2UHq2vk)


### Programming
- [X] udemy Flask course **IN PROGRESS**
- [ ] Learn Python3 from Scratch book

### Performance
- [ ] Linux Performance [documentation](http://www.brendangregg.com/linuxperf.html) by Brendan Gregg
- [X] Checklist [talk](https://youtu.be/zxCWXNigDpA) by Brendan Gregg
- [ ] [book](http://www.brendangregg.com/systems-performance-2nd-edition-book.html): System Performance: Enterprise and the Cloud 2nd edition by Brendan Gregg

### Monitoring
- [ ] Prometheus

### Chaos Engineering
- [ ] Nora Jones written book


### Cloud Providers
- [X] AWS Architect course
- [ ] AWS SysOps course
- [ ] exploring GCP


### misc.
- [ ] git
- [ ] containers and orchestration
- [ ] [learn Docker with NodeJS](https://www.udemy.com/course/docker-mastery-for-nodejs/)
- [ ] web servers


### future topics
- [ ] eBPF
- [ ] micro-VMs




## Practical Projects
- [ ] set up Prometheus
- [ ] configure WordPress from scratch
- [ ] CI/CD Pipeline workflow
- [ ] AWS ECS infrastructure template
- [ ] AWS static site template
- [ ] explore GCP with the [Linux Upskill Challenge](https://github.com/snori74/linuxupskillchallenge) **IN PROGRESS**


## Research Projects
- [ ] create an OS from scratch
- [ ] create a web server from scratch


## Additional Computer Science topics

- [my Computer Science Curriculum](https://gitlab.com/cdnuzzo/learn-computer-science)

## Additional resources

- [LinkedIn's School of SRE](https://linkedin.github.io/school-of-sre/)
- [SRE Interview Prep Guide](https://github.com/mxssl/sre-interview-prep-guide)
